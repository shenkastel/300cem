package com.shenkastel.daniel.towit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.daniel.towit.R;

public class MainActivity extends AppCompatActivity{
        private Button mDriver, mCustomer;
        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            mDriver = (Button) findViewById(R.id.driver);
            mCustomer = (Button) findViewById(R.id.customer);

            startService(new Intent(MainActivity.this, onAppKilled.class));
            mDriver.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, driverLoginActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
            });

            mCustomer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(MainActivity.this, customerLoginActivity.class);
                    startActivity(intent);
                    finish();
                    return;
                }
            });
        }
}
